---
title: "նախագծեր"
date: 2020-03-28T13:09:53+04:00
draft: false
---


նախագիծ         |       կոդ | տեղական հանգոյց
|---    |---    |--- 
|
|[Diaspora](https://diasporafoundation.org/)    |[AGPLv3](https://github.com/diaspora/diaspora)|[սփիւռք](https://spyurk.am/)
|[mastodon](https://joinmastodon.org/)| [AGPLv3](https://github.com/tootsuite/mastodon)| [թութ](https://թութ.հայ/about)
|[Matrix (Synapse)](https://matrix.org/)       |[Apache 2.0](https://github.com/matrix-org/synapse)|[մատրիքս](https://matrix.am)
|[Pleroma](pleroma.social)|	[AGPLv3](https://git.pleroma.social/pleroma/pleroma)
|[peertube](joinpeertube.org)| 	[AGPLv3](https://github.com/Chocobozzz/PeerTube)
|[writefreely](writefreely.org)| [AGPLv3](https://github.com/writeas/writefreely)
|[pixelfed](pixelfed.org)| 	[AGPLv3](https://github.com/pixelfed/pixelfed)|[կէտ.հայ](https://կէտ.հայ/)
|[Hubzilla](hubzilla.org)| 	[MIT](https://framagit.org/hubzilla/core)
|[Friendica](friendi.ca)|	[AGPLv3](https://github.com/friendica/friendica)
|[WordPress](wordpress.org)| 	[GPLv2](https://wordpress.org/download/source/)
|[Misskey](misskey.xyz)| 	[AGPLv3](https://github.com/syuilo/misskey)
|[Prosody](prosody.im)| 	[MIT](https://hg.prosody.im/trunk)
|[Plume](joinplu.me)| 	[AGPLv3](https://github.com/Plume-org/Plume)
|[Funkwhale](funkwhale.audio)| 	[AGPLv3](https://code.eliotberriot.com/users/sign_in)
|[ActivityRelay](git.pleroma.social/pleroma/relay)| 	[AGPLv3](https://git.pleroma.social/pleroma/relay)
|[GNU social](gnu.io/social)| 	[AGPLv3](https://notabug.org/diogo/gnu-social)
|[microblogpub](microblog.pub)| 	[AGPLv3](https://github.com/tsileo/microblog.pub)
|[zap](zotlabs.com/zap)| 	[MIT](https://framagit.org/zot/zap)
|[mobilizon](joinmobilizon.org/en)|	[AGPLv3](https://framagit.org/framasoft/mobilizon)
|[Socialhome](socialhome.network)| 	[AGPLv3](https://github.com/jaywink/socialhome)
|[Prismo](gitlab.com/mbajur/prismo)| 	[AGPLv3](https://gitlab.com/mbajur/prismo)
|[gancio](gancio.org)| 	[AGPLv3](https://framagit.org/les/gancio)
|[reel2bits](reel2bits.org)| 	[MIT](https://github.com/reel2bits/reel2bits)
|[p3k](indieweb.org/p3k)| 	[MIT](https://github.com/aaronpk/p3k)
 
